# --- Day 2: Inventory Management System ---
# https://adventofcode.com/2018/day/2
from collections import Counter

with open('Day2_input.txt') as input:
    ids = input.readlines()

count_2 = 0
count_3 = 0

for i in ids:
    counted = Counter(i)
    seen = 0
    for k,v in counted.items():
        if seen!=v & v==2:
            count_2 += 1
            seen = v
        if seen!=v & v==3:
            count_3 += 1
            seen = v

checksum = count_2 * count_3

print(checksum)


#---Part 2----
import difflib

box1 = ''
box2 = ''

for id1 in ids:
    for id2 in ids:
        if id1 == id2:
            break
        counter = 0
        for i in range(0, len(id1)):
            if id1[i] != id2[i]:
                counter+=1
            if counter > 1:
                break
        if counter == 1:
            box1 = id1
            box2 = id2

same = set(box1) & set(box2)
diff = ''

for c in box1:
    if not (c in same):
        diff = c

print(box1.replace(diff, ''))