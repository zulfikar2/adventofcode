# --- Day 6: Chronal Coordinates ---
# https://adventofcode.com/2018/day/5

import operator
from collections import Counter 

with open('Day6_input.txt') as input:
    in_coords = input.read().splitlines()

coords = []

for coord in in_coords:
    coords.append((int(coord.split(',')[0]), int(coord.split(',')[1].strip())))

box_top = min(coords, key=operator.itemgetter(1))[1]
box_right = max(coords, key=operator.itemgetter(0))[0]
box_bot = max(coords,key=operator.itemgetter(1))[1]
box_left = min(coords,key=operator.itemgetter(0))[0]

def is_finite(p):
    return box_left < p[0] < box_right and box_top < p[1] < box_bot

def manhattan_distance(p1,p2):
    #return sum(abs(a-b) for a,b in zip(p1,p2))
    return abs(p2[0] - p1[0]) + abs(p2[1] - p1[1])

def getMinIndex(dist):
    return dist.index(min(dist))

def oneMinOnly(dist):
    d = sorted(dist)
    return d[0] != d[1]

count = Counter()
inf_ids = set()
region = 0
for x in range(box_left, box_right+1):
    for y in range(box_top, box_bot+1):
        curr_pt = (x,y)
        dist = [manhattan_distance(curr_pt,pt) for pt in coords]
        if sum(dist) < 10000:
            region += 1
        if oneMinOnly(dist):
            if x == box_left or x == box_right or y == box_top or y == box_bot:
                inf_ids.add(coords[getMinIndex(dist)])
            count[coords[getMinIndex(dist)]] += 1

max_area = 0

for pt in coords:
    if is_finite(pt):
        if not pt in inf_ids:
            max_area = max(max_area, count[pt])

print(max_area)
print(region)