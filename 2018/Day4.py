# --- Day 3: No Matter How You Slice It ---
# https://adventofcode.com/2018/day/4
import re

class Guard:
    def __init__(self, id):
        self.id = id
        self.sleep_time = 0
        self.sleep = [0 for y in range(60)]

guards = []
known_id = []

with open('Day4_input.txt') as input:
    records = input.readlines()

def identify_guards(records):
    records.sort()
    for record in records:
        guard_id_search = re.search("Guard #([0-9]+)", record)
        if guard_id_search:
            guard_id = int(guard_id_search.group(1))
            if not guard_id in known_id:
                guards.append(Guard(guard_id))
                known_id.append(guard_id)
    
def get_sleep_time():
        asleep = False
        sleep_time = -1
        wake_time = -1
        curr_guard_id = -1
        for record in records:
            #if asleep, next is wake up record.
            if asleep:
                wake_search = re.search("\:([0-9]+)\]\swakes up", record)
                if wake_search:
                    asleep = False
                    wake_time = int(wake_search.group(1))
                    add_sleep(curr_guard_id, sleep_time, wake_time)
            #if awake, next is new guard record or fall asleep record
            else:
                asleep_search = re.search("\:([0-9]+)\]\sfalls asleep", record)
                if asleep_search:
                    asleep = True
                    sleep_time = int(asleep_search.group(1))
                else:
                    guard_id_search = re.search("Guard #([0-9]+)", record)
                    curr_guard_id = int(guard_id_search.group(1))
                
def add_sleep(id, sleep_time, wake_time):
    minutes = wake_time - sleep_time
    for guard in guards:
        if id == guard.id:
            guard.sleep_time += minutes
            for t in range(sleep_time, wake_time):
                guard.sleep[t] += 1
            return

def most_sleep():
    max_sleep = -1
    guard_id = -1
    max_sleep_minute = -1
    max_sleep_at_minute= -1
    for guard in guards:
        if guard.sleep_time > max_sleep:
            max_sleep = guard.sleep_time
            guard_id = guard.id
    for guard in guards:
        if guard_id == guard.id:
            for t in range(60):
                if guard.sleep[t] > max_sleep_at_minute:
                    max_sleep_at_minute = guard.sleep[t]
                    max_sleep_minute = t
    return [guard_id, max_sleep_minute]

def most_sleep_minute():
    max_sleep_at_minute = -1
    max_sleep_minute = -1
    guard_id = -1
    for guard in guards:
        for t in range(60):
            if guard.sleep[t] > max_sleep_at_minute:
                max_sleep_at_minute = guard.sleep[t]
                max_sleep_minute = t
                guard_id = guard.id
    return [max_sleep_minute, guard_id]

identify_guards(records)
get_sleep_time()

print("Guard List with ID and sleep time")
for guard in guards: print(guard.id, guard.sleep_time)
max_sleeper = most_sleep()
print("Guard ID with most sleep : "  + str(max_sleeper[0]))
print("Highest sleep minute : "  + str(max_sleeper[1]))

print(max_sleeper[0] * max_sleeper[1])

#-----PART 2-----

max_minute_sleeper = most_sleep_minute()
print(max_minute_sleeper[0], max_minute_sleeper[1])
print(max_minute_sleeper[0] * max_minute_sleeper[1])