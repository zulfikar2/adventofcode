# --- Day 7: The Sum of Its Parts ---
# https://adventofcode.com/2018/day/5

import re
from collections import Counter

with open('Day7_input.txt') as input:
    steps = input.read().splitlines()

stepA = set()
stepB = set()
completed = []
count = Counter()
toDo = dict()
available = set()

for step in steps:
    #X before Y
    X = re.search("Step ([A-Z])", step)
    Y = re.search("step ([A-Z])", step)
    stepA.add(X.group(1))
    stepB.add(Y.group(1))
    if not (X.group(1) in toDo):
        toDo[X.group(1)] = list(Y.group(1))
        count[Y.group(1)] += 1
    else:
        toDo[X.group(1)] = toDo[X.group(1)] + list(Y.group(1))
        count[Y.group(1)] += 1

starters = stepA - stepB

#print(toDo)

def Alphabetical_First(alphers):
    start = alphers.pop()
    for s in starters:
        if start > s:
            start = s
    return start

def complete_step(step):
    print("Doing : " + str(step))
    for s in toDo:
        if step == s:
            print(toDo)
            print(completed)
            print(available)
            td = sorted(toDo[step])
            if len(td) == 1:
                del toDo[step]
            else:
                toDo[step] = td[1:]
            if not td[0] in step_order:
                if count[td[0]] == 1:
                    step_order.append(td[0])
                    completed.append(td[0])
                count[td[0]] -= 1
            break
    if not (step in toDo):
        completed.remove(step)

step_order = []

if len(starters) == 1:
    step_order.append(starters.pop())
else:
    step_order.append(Alphabetical_First(starters))

completed.append(step_order[0])

import time

while len(toDo) >= 1:
    completed.sort()
    for c in completed:
        complete_step(c)
        break
    if len(completed) == 0:
        #pick first alpha to do
        do_alpha = 'zzz'
        for x in toDo:
            if do_alpha > x:
                do_alpha = x
        for s in toDo[do_alpha]:
            available.add(s)
            if do_alpha > s:
                do_alpha = s
        completed.append(do_alpha)
        step_order.append(do_alpha)
        
        complete_step(do_alpha)

            
    #print(count)
    #print(toDo)
    #print(completed)
    print(str("".join(step_order)))
    #time.sleep(1)

#print(completed)
print(str("".join(step_order)))