# --- Day 3: No Matter How You Slice It ---
# https://adventofcode.com/2018/day/3
import numpy as np

fabric_size_x = 1000
fabric_size_y = 1000
#fabric = np.zeros((fabric_size_x, fabric_size_y))
fabric = [[[] for x in range(fabric_size_x)] for y in range(fabric_size_y)]
claims = []
claim_list = []
overlapped_claims = []

with open('Day3_input.txt') as input:
    in_claims = input.readlines()

class Claim:
    def __init__(self, claim_num, x, y, size_x, size_y):
        self.claim_num = claim_num
        self.x1 = x
        self.y1 = y
        self.size_x = size_x
        self.size_y = size_y
        self.x2 = (self.x1-1)+size_x
        self.y2 = (self.y1-1)+size_y

    def modifyFabric(self):
        claim_list.append(self.claim_num)
        for x in range(self.size_x):
            for y in range(self.size_y):
                fabric[self.x1+x][self.y1+y].append(self.claim_num)



def clean_input(claims_list):
    for claim in claims_list:
        #Some dumb splitting. Regex woulda been easier, but I already wrote this    
        claim_num = int(claim.split('#')[1].split('@')[0])
        x = int(claim.split('#')[1].split('@')[1].split(',')[0])
        y = int(claim.split('#')[1].split('@')[1].split(',')[1].split(':')[0])
        size_x = int(claim.split('#')[1].split('@')[1].split(',')[1].split(':')[1].split('x')[0])
        size_y = int(claim.split('#')[1].split('@')[1].split(',')[1].split(':')[1].split('x')[1])
        claims.append(Claim(claim_num, x, y, size_x, size_y))


clean_input(in_claims)

for claim in claims:
    claim.modifyFabric()

total_area = 0
unique_claim = []

for x in range(fabric_size_x):
    for y in range(fabric_size_y):
        if len(fabric[x][y]) >= 2:
            total_area += 1
            overlapped_claims.append(fabric[x][y])

sum_overlapped_claims = []

for claimed in overlapped_claims:
    for indiv_claim in claimed:
        sum_overlapped_claims.append(indiv_claim)

unique_claim = list(set(claim_list) - set(sum_overlapped_claims))

print(total_area)
print(unique_claim)
#print(fabric)