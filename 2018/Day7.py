import networkx as nx
from string import ascii_uppercase

Graph = nx.DiGraph()
Graph.add_edges_from([(x.split()[1], x.split()[7]) for x in open('Day7_input.txt').readlines()])

#order = ''.join(nx.lexicographical_topological_sort(Graph))
#print(order)

#-----PART 2-----

workers = 5
durations = {letter:ord(letter)-4 for letter in ascii_uppercase}
print(durations)
print(sum(durations[n] for n in nx.dag_longest_path(Graph)))