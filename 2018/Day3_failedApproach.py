# --- Day 3: No Matter How You Slice It ---
# https://adventofcode.com/2018/day/2

with open('Day3_input.txt') as input:
    claims = input.readlines()

class Claim:
    def __init__(self, claim_num, x, y, size_x, size_y):
        self.claim_num = claim_num
        self.x1 = x
        self.y1 = y
        self.size_x = size_x
        self.size_y = size_y
        self.x2 = (self.x1-1)+size_x
        self.y2 = (self.y1-1)+size_y

    def overlap(self, B):
            #Assume A is self
        #print("x1 : " + str(self.x1) + " B : " + str(B.x1))
        #print("y1 : " + str(self.y1) + " B : " + str(B.y1))
            # If A's left edge is to the left of the B's right edge, - then A is to left Of B's left edge
        if (self.x1 < B.x2 and
            # If A's right edge is to the right of the B's left edge, - then A is to right of B's right edge
            self.x2 > B.x1 and
            # If A's top edge is Above B's bottom edge, - then A is above B
            self.y1 < B.y2 and
            # If A's bottom edge is below B's top edge, - then A is below B
            self.y2 > B.y1):
            return True
        return False

    def getAreaClaim(self, B):
        #print("x1 : " + str(self.x1) + " B : " + str(B.x1))
        o_x1 = max(self.x1, B.x1)
        #print("y1 : " + str(self.y1) + " B : " + str(B.y1))
        o_y1 = max(self.y1, B.y1)
        o_size_x = min(self.x2, B.x2) - o_x1 + 1
        o_size_y = min(self.y2, B.y2) - o_y1 + 1
        return Claim(-1, o_x1, o_y1, o_size_x, o_size_y)


overlap_claims = [Claim(0,0,0,0,0)]
claimed = []

for claim1 in claims:
    #Some dumb splitting. Regex woulda been easier, but I already wrote this    
    claim_num = int(claim1.split('#')[1].split('@')[0])
    x = int(claim1.split('#')[1].split('@')[1].split(',')[0]) + 1
    y = int(claim1.split('#')[1].split('@')[1].split(',')[1].split(':')[0]) + 1
    size_x = int(claim1.split('#')[1].split('@')[1].split(',')[1].split(':')[1].split('x')[0])
    size_y = int(claim1.split('#')[1].split('@')[1].split(',')[1].split(':')[1].split('x')[1])

    r1 = Claim(claim_num,x,y,size_x,size_y)

    for claim2 in claims:
        claim_num2 = int(claim2.split('#')[1].split('@')[0])
        #skip matching claims to itself
        if claim_num == claim_num2:
            continue
        x2 = int(claim2.split('#')[1].split('@')[1].split(',')[0]) + 1
        y2 = int(claim2.split('#')[1].split('@')[1].split(',')[1].split(':')[0]) + 1
        size_x2 = int(claim2.split('#')[1].split('@')[1].split(',')[1].split(':')[1].split('x')[0])
        size_y2 = int(claim2.split('#')[1].split('@')[1].split(',')[1].split(':')[1].split('x')[1])

        r2 = Claim(claim_num2,x2,y2,size_x2,size_y2)

        if r1.overlap(r2):
            print("Overlapping : " + str(claim_num) + " " + str(claim_num2))
            #No Overlap area has been set yet, set one
            if overlap_claims[0].size_x == 0:
                overlap_claims[0] = r1.getAreaClaim(r2)
                print("Added")
                claimed.append((r1.claim_num, r2.claim_num))
                print(claimed)
            else:
                #check duplicate
                if ((r1.claim_num, r2.claim_num) not in claimed) and ((r2.claim_num, r1.claim_num) not in claimed):
                    overlap_claims.append(r1.getAreaClaim(r2))
                    print("Added")
                    claimed.append((r1.claim_num, r2.claim_num))
                    print(claimed)
            #Overlap has already been set, check if new overlap overlaps. If not, add to overlap array?
    

total_area = 0

for c in overlap_claims:
    total_area += c.size_x * c.size_y

print(total_area)

