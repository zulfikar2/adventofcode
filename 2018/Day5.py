# --- Day 5: Alchemical Reduction ---
# https://adventofcode.com/2018/day/5

with open('Day5_input.txt') as input:
    polymer = input.read().splitlines()

def react(poly, index):
    if index >= len(poly)-1:
        return "ERROR INDEXING"
    #print("Checking : " + str(poly[index] + " " + str(poly[index+1])))
    #aA
    if poly[index].islower() and poly[index+1].isupper() and poly[index].upper() == poly[index+1]:
        return "remove"
    #Aa
    if poly[index].isupper() and poly[index+1].islower() and poly[index].lower() == poly[index+1]:
        return "remove"

    return "NO ACTION"

def React(polymer):
    while(True):
        checksum = 0
        action = ""
        for x in range(0, len(polymer)-1):
            action = react(polymer, x)
            #print(action)
            if action == "remove":
                polymer = polymer[:x] + polymer[x+2:]
                #print(polymer)
                break
            elif action == "NO ACTION":
                checksum += 1
        if checksum == len(polymer)-1 or action == "ERROR INDEXING":
            break
    return polymer

#polymer = React(polymer)
#print(len(polymer))

#-----PART 2-----
from string import ascii_lowercase

def react2(polymer):
	i = 1
	while i < len(polymer):
		a = polymer[i-1]
		b = polymer[i]		
		if a != b and a.upper() == b.upper():
			del polymer[i-1:i+1]
			if i > 1: i = i - 1
		else: i = i + 1
	return polymer

reacted = react2(list(polymer[0]))

print(min(len(react2([x for x in reacted if x.lower() != c])) for c in ascii_lowercase))