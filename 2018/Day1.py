# --- Day 1: Chronal Calibration ---
# https://adventofcode.com/2018/day/1

with open('Day1_input.txt') as input:
    frequency = input.readlines()

cleanedUp_freq = []
for freq in frequency:
    cleanedUp_freq.append(int(freq))
    
curr_freq = sum(cleanedUp_freq)

print(curr_freq)

#---PART 2----

from itertools import accumulate, cycle

seen = set({0})
print(next(f for f in accumulate(cycle(cleanedUp_freq)) if f in seen or seen.add(f)))
