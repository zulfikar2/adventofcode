# --- Day 7: The Sum of Its Parts ---
# https://adventofcode.com/2018/day/5

import re

with open('Day7_input.txt') as input:
    steps = input.read().splitlines()

class Graph: 
    def __init__(self): 
        self.nodes = []
  
    def addEdge(self,u,v): 
        for n in self.nodes:
            if n.name == v:
                n.req.append(u)
                return
        self.nodes.append(Node(v,u))

class Node:
    def __init__(self, name, req):
        self.name = name
        self.req = list(req)

def getAlpha(alpha):
    alpha_s = 'zzz'
    for s in alpha:
        if alpha_s > s:
            alpha_s = s
    return alpha_s

stepA = set()
stepB = set()
g = Graph()

for step in steps:
    #X before Y
    X = re.search("Step ([A-Z])", step)
    Y = re.search("step ([A-Z])", step)
    stepA.add(X.group(1))
    stepB.add(Y.group(1))
    g.addEdge(X.group(1),Y.group(1))

starters = stepA - stepB
#print(starters)
for n in g.nodes:
    print(n.name, n.req)

completed = []
available = set(starters)

print("Completed : " + str(completed))
print("Available : " + str(available))

while len(available) > 0:
    step = getAlpha(available)
    print("Doing : " + str(step))
    completed.append(step)
    available = available - set(step)
    for n in g.nodes:
        print("Checking : " + str(n.name))
        full = True
        for r in n.req:
            if not (r in completed):
                full = False
                break
        if full:
            print("AVAIABLE : " + str(n.name))
            available.add(n.name)
            for d in g.nodes:
                if d.name == n.name:
                    g.nodes.remove(d)
                    break
    #print(available)

print(str("".join(completed)))