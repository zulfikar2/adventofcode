using System.Globalization;
using System.Text.RegularExpressions;
using System.Linq;

namespace AdventOfCode2023
{
    class Day3
    {
        public void Start()
        {
            Console.WriteLine("Running Day 2");
            Part1("Day3/Day3.txt");
            Part2("Day3/Day3.txt");
        }

        public void Part1(string filePath)
        {
            var rows = File.ReadAllLines(filePath);
            int Sum = 0;
            var symbols = ParseEngineSchematics(rows, new Regex(@"[^.0-9]"));
            var nums = ParseEngineSchematics(rows, new Regex(@"\d+"));

            var AdjNums = from n in nums
                          where symbols.Any(s => isAdjacent(s, n))
                          select n.part;

            Sum = AdjNums.Sum();

            Console.WriteLine("Answer P1: " + Sum.ToString());
        }

        public void Part2(string filePath)
        {
            var rows = File.ReadAllLines(filePath);
            int Sum = 0;
            var gears = ParseEngineSchematics(rows, new Regex(@"\*"));
            var nums = ParseEngineSchematics(rows, new Regex(@"\d+"));

            var AdjNums = from g in gears
                          let neighbour = from n in nums
                                          where isAdjacent(n, g)
                                          select n.part
                          where neighbour.Count() == 2
                          select neighbour.First() * neighbour.Last();

            Sum = AdjNums.Sum();

            Console.WriteLine("Answer P2: " + Sum.ToString());
        }

        EnginePart[] ParseEngineSchematics(string[] rows, Regex rx) => (
            from row in Enumerable.Range(0, rows.Length)
            from match in rx.Matches(rows[row])
            select new EnginePart(match.Value, row, match.Index)
        ).ToArray();



        bool isAdjacent(EnginePart ep1, EnginePart ep2) =>
            Math.Abs(ep2.row - ep1.row) <= 1 &&
            ep1.col <= ep2.col + ep2.item.Length &&
            ep2.col <= ep1.col + ep1.item.Length;

        record EnginePart(string item, int row, int col)
        {
            public int part => int.Parse(item);
        }
    }
}