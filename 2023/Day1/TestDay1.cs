using AdventOfCode2023;
using Xunit;
using Xunit.Abstractions;

public class TestDay1
{
    private readonly ITestOutputHelper output;
    readonly Day1 day1 = new();

    public TestDay1(ITestOutputHelper output)
    {
        this.output = output;
    }

    [Fact]
    void CalibrateTest_P1()
    {
        var Testcalibrations = File.ReadAllLines("../../../Day1/Day1_testP1.txt");
        string regX = @"\d";
        List<int> calibrations_nums = new List<int>();
        foreach (String line in Testcalibrations)
        {
            output.WriteLine("Got line : " + line);
            int numberFound = day1.getCalibrate(line, regX);
            output.WriteLine(numberFound.ToString());
            calibrations_nums.Add(numberFound);
        }
        Assert.Equal(291, calibrations_nums.Sum());
    }

    [Fact]
    void CalibrateTest_P2()
    {
        var Testcalibrations = File.ReadAllLines("../../../Day1/Day1_testP2.txt");
        string regX = @"\d|one|two|three|four|five|six|seven|eight|nine";
        List<int> calibrations_nums = new List<int>();
        foreach (String line in Testcalibrations)
        {
            output.WriteLine("Got line : " + line);
            int numberFound = day1.getCalibrate(line, regX);
            output.WriteLine(numberFound.ToString());
            calibrations_nums.Add(numberFound);
        }
        Assert.Equal(384, calibrations_nums.Sum());
    }
}