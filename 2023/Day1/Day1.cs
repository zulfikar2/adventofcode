using System.Collections;
using System.Text.RegularExpressions;
using Xunit.Sdk;

namespace AdventOfCode2023
{
	class Day1
	{
		public void Start()
		{
			Console.WriteLine("Running Day 1");
			Part1("Day1/Day1_P1.txt");
			Part2("Day1/Day1_P2.txt");
		}

		public void Part1(string filePath)
		{
			var calibrations = File.ReadAllLines("Day1/Day1_P1.txt");
			string regX = @"\d";
			List<int> calibrations_nums = new List<int>();
			foreach (String line in calibrations)
			{
				calibrations_nums.Add(getCalibrate(line, regX));
			}
			Console.WriteLine("Answer P1: " + calibrations_nums.Sum().ToString());
		}

		public void Part2(string filePath)
		{
			var calibrations = File.ReadAllLines(filePath);
			List<int> calibrations_nums = new List<int>();
			string regX = @"\d|one|two|three|four|five|six|seven|eight|nine";
			foreach (String line in calibrations)
			{
				calibrations_nums.Add(getCalibrate(line, regX));
			}
			Console.WriteLine("Answer P2: " + calibrations_nums.Sum().ToString());
		}

		public int getCalibrate(string line, string Regx)
		{
			return Int32.Parse(ParseNum(Regex.Match(line, Regx).Value) + ParseNum(Regex.Match(line, Regx, RegexOptions.RightToLeft).Value));
		}

		string ParseNum(string num) => num switch
		{
			"one" => "1",
			"two" => "2",
			"three" => "3",
			"four" => "4",
			"five" => "5",
			"six" => "6",
			"seven" => "7",
			"eight" => "8",
			"nine" => "9",
			_ => num
		};
	}
}