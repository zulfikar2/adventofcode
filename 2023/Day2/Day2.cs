using System.Text.RegularExpressions;

namespace AdventOfCode2023
{
    class Day2
    {
        public void Start()
        {
            Console.WriteLine("Running Day 2");
            Part1("Day2/Day2_P1.txt");
            Part2("Day2/Day2_P2.txt");
        }

        public void Part1(string filePath)
        {
            var games = File.ReadAllLines(filePath);
            int Sum = 0;
            foreach (string game in games)
            {
                bool validGame = false;
                foreach (string set in GetSets(game))
                {
                    var rolled = GetRolls(set);
                    if (ValidGame(rolled.red, rolled.blue, rolled.green))
                    {
                        validGame = true;
                    }
                    else
                    {
                        validGame = false;
                        break;
                    }
                }
                if (validGame) { Sum += GetGame(game); }
            }
            Console.WriteLine("Answer P1: " + Sum.ToString());
        }

        public void Part2(string filePath)
        {
            var games = File.ReadAllLines(filePath);
            int Sum = 0;
            foreach (string game in games)
            {
                int red = 0, blue = 0, green = 0;
                foreach (string set in GetSets(game))
                {
                    var rolled = GetRolls(set);
                    if (rolled.red > red) { red = rolled.red; }
                    if (rolled.blue > blue) { blue = rolled.blue; }
                    if (rolled.green > green) { green = rolled.green; }
                }
                Sum += red * blue * green;
            }
            Console.WriteLine("Answer P2: " + Sum.ToString());
        }

        public bool ValidGame(int red, int blue, int green,
        int ValidRed = 12, int ValidBlue = 14, int ValidGreen = 13)
        {
            return red <= ValidRed && green <= ValidGreen && blue <= ValidBlue;
        }

        public string[] GetSets(string line)
        {
            string game = line.Split(":")[1];
            return game.Split(";");
        }

        public int GetGame(string line)
        {
            return Int32.Parse(Regex.Match(line.Split(":")[0], @"\d+").Value);
        }

        public (int red, int blue, int green) GetRolls(string line)
        {
            int red = 0, blue = 0, green = 0;
            string[] rolled = line.Split(",");
            foreach (string roll in rolled)
            {
                switch (roll.Trim())
                {
                    case string r when r.Contains("red"): red = GetGame(roll); break;
                    case string g when g.Contains("green"): green = GetGame(roll); break;
                    case string b when b.Contains("blue"): blue = GetGame(roll); break;
                }
            }
            return (red, blue, green);
        }
    }
}