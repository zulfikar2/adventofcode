using AdventOfCode2023;
using NuGet.Frameworks;
using Xunit;
using Xunit.Abstractions;

public class TestDay2
{
    private readonly ITestOutputHelper output;
    readonly Day2 day2 = new();

    public TestDay2(ITestOutputHelper output)
    {
        this.output = output;
    }

    [Fact]
    void Test_P1()
    {
        var games = File.ReadAllLines("../../../Day2/Day2_testP1.txt");
        int Sum = 0;
        foreach (string game in games)
        {
            int gameID = day2.GetGame(game);
            bool validGame = false;
            foreach (string set in day2.GetSets(game))
            {
                output.WriteLine("Checking game " + gameID + " for set " + set);
                var (red, blue, green) = day2.GetRolls(set);
                output.WriteLine("red: " + red + " blue: " + blue + " green: " + green);
                if (day2.ValidGame(red, blue, green))
                {
                    validGame = true;
                }
                else
                {
                    output.WriteLine("INVALID set");
                    validGame = false;
                    break;
                }
            }
            if (validGame) { Sum += gameID; }
        }
        Assert.Equal(8, Sum);
    }

    [Fact]
    void Test_P2()
    {
        var games = File.ReadAllLines("../../../Day2/Day2_testP2.txt");
        int Sum = 0;
        foreach (string game in games)
        {
            int red = 0, blue = 0, green = 0;
            foreach (string set in day2.GetSets(game))
            {
                output.WriteLine("Checking set " + set);
                var rolled = day2.GetRolls(set);
                output.WriteLine("red: " + red + " blue: " + blue + " green: " + green);
                if (rolled.red > red) { red = rolled.red; }
                if (rolled.blue > blue) { blue = rolled.blue; }
                if (rolled.green > green) { green = rolled.green; }
            }
            Sum += red * blue * green;
        }
        Assert.Equal(2286, Sum);
    }
}