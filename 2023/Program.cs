﻿using System.Text.RegularExpressions;
using AdventOfCode2023;

if (args.Length == 0) { Console.Error.Write("ERROR : No arguments found."); return; }
if (!Regex.Match(args[0], "^day[0-9]{1,2}").Success)
{
    Console.Error.Write("ERROR : Arguments are incorrect"); return;
}

int dayToRun = Int32.Parse(args[0].ToCharArray().Last().ToString());
RunDay(dayToRun);

static void RunDay(int day)
{
    var methods = new Dictionary<int, Action>()
            {
                {1, new Day1().Start},
                {2, new Day2().Start},
                {3, new Day3().Start},
                {4, new Day4().Start}
            };
    if (methods.ContainsKey(day))
    {
        methods[day]();
    }
    else
    {
        Console.Error.Write("ERROR : Day does not exist or not yet coded!"); return;
    }
}