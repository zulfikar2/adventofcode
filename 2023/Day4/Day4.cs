namespace AdventOfCode2023
{
    class Day4
    {
        public void Start()
        {
            Console.WriteLine("Running Day 2");
            Part1("Day4/Day4.txt");
            Part2("Day4/Day4.txt");
        }

        void Part1(string filePath)
        {
            var cards = File.ReadAllLines(filePath);
            int winCount = 0, pts = 0;
            foreach (string line in cards)
            {
                winCount = WinningNumberCount(GetCards(line));
                if (winCount != 0) { pts += (int)Math.Pow(2, winCount - 1); }
            }
            Console.WriteLine("Answer P1: " + pts.ToString());
        }

        void Part2(string filePath)
        {
            var cards = File.ReadAllLines(filePath);
            var card_count = cards.Select(_ => 1).ToArray();
            for (int i = 0; i < cards.Count(); i++)
            {
                string line = cards[i];
                int winCount = WinningNumberCount(GetCards(line));
                var (card, count) = (line, card_count[i]);
                for (int x = 0; x < winCount; x++)
                {
                    card_count[i + x + 1] += count;
                }
            }
            Console.WriteLine("Answer P1: " + card_count.Sum());
        }

        string[] GetCards(string line)
        {
            string cards = line.Split(":")[1];
            return cards.Split("|");
        }

        int WinningNumberCount(string[] cards)
        {
            string[] card1 = cards[0].Split(" ").Where(x => !string.IsNullOrEmpty(x)).ToArray();
            string[] card2 = cards[1].Split(" ").Where(x => !string.IsNullOrEmpty(x)).ToArray();
            var winningNums = card1.Where(num2 => card2.Any(num1 => num2.Equals(num1)));
            return winningNums.Count();
        }

    }
}

